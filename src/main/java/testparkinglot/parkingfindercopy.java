package testparkinglot;

import java.util.*;

public class parkingfindercopy{

	// List<Nearestspot> parkingindexlarge;
	// List<Nearestspot> parkingindexmedium;
	// List<Nearestspot> parkingindexsmall;
	// [TODO] Code to interfaces always; instead of declaring something as
	// Hashmap; declare that as Map
	// [TODO] The name NearestSpot doesn't sound appropriate here. NearestSpot
	// is typically an outcome of a algo logic
	HashMap<String, List<Nearestspot>> parking = new HashMap<String, List<Nearestspot>>();
	List<Nearestspot> fullparking = new ArrayList<Nearestspot>();

	public parkingfindercopy(List<Parkingspot> parkingspots) {

		parking.put("Large", getLargeParkingSpots(parkingspots));
		parking.put("Medium", getMediumParkingSpots(parkingspots));
		parking.put("Small", getSmallParkingSpots(parkingspots));
	}

	private List<Nearestspot> getSmallParkingSpots(List<Parkingspot> parkingspots) {
		// TODO You can try java-8 filter expression on list
		List<Nearestspot> smallspots = new ArrayList<Nearestspot>();
		for (int i = 0; i < parkingspots.size(); i++) {
			Parkingspot pspot = parkingspots.get(i);
			if (pspot.getsize().equals("Small")) {
				Nearestspot nearspot = new Nearestspot(pspot);
				smallspots.add(nearspot);
			}
		}
		return smallspots;
	}

	private List<Nearestspot> getMediumParkingSpots(List<Parkingspot> parkingspots) {
		List<Nearestspot> mediumspots = new ArrayList<Nearestspot>();
		for (int i = 0; i < parkingspots.size(); i++) {
			Parkingspot pspot = parkingspots.get(i);
			if (pspot.getsize().equals("Medium")) {
				Nearestspot nearspot = new Nearestspot(pspot);
				mediumspots.add(nearspot);
			}
		}
		return mediumspots;
	}

	private List<Nearestspot> getLargeParkingSpots(List<Parkingspot> parkingspots) {
		List<Nearestspot> largeSpots = new ArrayList<Nearestspot>();
		for (int i = 0; i < parkingspots.size(); i++) {
			Parkingspot pspot = parkingspots.get(i);
			if (pspot.getsize().equals("Large")) {
				Nearestspot nearspot = new Nearestspot(pspot);
				largeSpots.add(nearspot);

			}
		}
		System.out.println("Here4" + largeSpots);
		return largeSpots;
	}

	public Parkingspot getnearestspot(String gate, String type) {
		List<Nearestspot> tmpparking;
		Parkingspot pspot = null;
		// [TODO] Think of using something like a chain-of-responsibility
		// pattern here
		if (type.equals("Large")) {
			tmpparking = parking.get("Large");
			pspot = getspot(type, "Large", gate, tmpparking);
		}
		if (type.equals("Medium")) {
			tmpparking = parking.get("Medium");
			pspot = getspot(type, "Medium", gate, tmpparking);
			if (pspot == null) {
				tmpparking = parking.get("Large");
				pspot = getspot(type, "Large", gate, tmpparking);
			}

		}
		if (type.equals("Small")) {
			tmpparking = parking.get("Small");
			pspot = getspot(type, "Small", gate, tmpparking);
			if (pspot == null) {
				tmpparking = parking.get("Medium");
				pspot = getspot(type, "Medium", gate, tmpparking);
			}
			if (pspot == null) {
				tmpparking = parking.get("Large");
				pspot = getspot(type, "Large", gate, tmpparking);
			}

		}

		/**
		 * for (int i=0;i<parkingindex.size();i++){ pspot =
		 * parkingindex.get(i).getParkingspot(); String spotsize =
		 * pspot.getsize(); if (spotsize.equals(type)){ available = true; return
		 * pspot; }
		 * 
		 * }
		 * 
		 * } if (gate.equals("A1")){ for (int i=0;i<parkingindex.size();i++){
		 * pspot = parkingindex.get(i).getParkingspot(); String spotsize =
		 * pspot.getsize(); if (spotsize.equals(type)){ return pspot; } } } if
		 * (gate.equals("A1")){ for (int i=0;i<parkingindex.size();i++){ pspot =
		 * parkingindex.get(i).getParkingspot(); String spotsize =
		 * pspot.getsize(); if (spotsize.equals(type)){ return pspot; } }
		 **/

		return pspot;
	}

	public Parkingspot getspot(String type, String listtype, String gate, List<Nearestspot> parkinglist) {

		Parkingspot pspot = null;
		System.out.println("Here2");
		// [TODO] Hard-coding the geographical layout of the parking area is not
		// a good idea; read it from some data-store
		if (gate.equals("A1")) {

			pspot = getinrange(type, listtype, "B01", "B10", parkinglist);
			if (pspot == null) {
				pspot = getinrange(type, listtype, "B20", "B30", parkinglist);
			}
			if (pspot == null) {
				pspot = getinrange(type, listtype, "B30", "B40", parkinglist);
			}
		}
		if (gate.equals("A2")) {

			pspot = getinrange(type, listtype, "B20", "B30", parkinglist);
			if (pspot == null) {
				pspot = getinrange(type, listtype, "B1", "B10", parkinglist);
			}
			if (pspot == null) {
				pspot = getinrange(type, listtype, "B30", "B40", parkinglist);
			}
		}
		if (gate.equals("A3")) {

			pspot = getinrange(type, listtype, "B30", "B40", parkinglist);
			if (pspot == null) {
				pspot = getinrange(type, listtype, "B20", "B30", parkinglist);
			}
			if (pspot == null) {
				pspot = getinrange(type, listtype, "B1", "B10", parkinglist);
			}
		}
		return pspot;
	}

	public Parkingspot getinrange(String type, String listtype, String lower, String upper,
			List<Nearestspot> parkinglist) {
		Parkingspot pspot = null;
		if (type.equals("Large")) {
			pspot = getspotinrange(lower, upper, parkinglist);
		}
		// [TODO] This entire if statements can be simplified by comparing type
		// and listType and calling getSpotInRange and getSpotInRangeAcross
		// otherwise
		// [TODO] typo on Medium----logic will fail --- better to declare such values as final strings (constants)
		if (type.equals("Meidum")) {
			if (listtype.equals("Medium")) {
				pspot = getspotinrange(lower, upper, parkinglist);
			}
			// [TODO] better to code this as else if instead of separate
			// if---more clear that way
			if (listtype.equals("Large")) {
				pspot = getspotinrangecross(lower, upper, parkinglist);
			}
		}
		if (type.equals("Small")) {
			if (listtype.equals("Small")) {
				pspot = getspotinrange(lower, upper, parkinglist);
			}
			if (listtype.equals("Large") || listtype.equals("Medium")) {
				pspot = getspotinrangecross2(lower, upper, parkinglist);
			}

		}
		return pspot;
	}

	public Parkingspot getspotinrange(String lower, String upper, List<Nearestspot> parkinglist) {
		System.out.println("Here3");
		String tmpspotid;
		Nearestspot nearspot;
		Parkingspot pspot = null;
		// [TODO] Iterate on list
		for (int i = 0; i < parkinglist.size(); i++) {
			nearspot = parkinglist.get(i);
			pspot = parkinglist.get(i).getParkingspot();
			tmpspotid = pspot.getparkingID();
			System.out.println("Here2" + tmpspotid + " ");
			if ((tmpspotid.compareTo(lower) >= 0) && (tmpspotid.compareTo(upper) <= 0)) {
				break;
			} else {
				pspot = null;
			}
		}

		return pspot;

	}

	// for medium to large
	public Parkingspot getspotinrangecross(String lower, String upper, List<Nearestspot> parkinglist) {

		String tmpspotid;
		Nearestspot nearspot;
		Parkingspot pspot = null;
		for (int i = 0; i < parkinglist.size(); i++) {
			pspot = parkinglist.get(i).getParkingspot();
			nearspot = parkinglist.get(i);
			tmpspotid = pspot.getparkingID();
			int tmpvehparked = nearspot.getnumvehparked();

			if ((tmpspotid.compareTo(lower) >= 0) && (tmpspotid.compareTo(upper) <= 0)) {
				if (tmpvehparked < 2) {
					break;
				}
			}
		}
		return pspot;
	}

	// for small to medium/Large
	public Parkingspot getspotinrangecross2(String lower, String upper, List<Nearestspot> parkinglist) {

		String tmpspotid;
		Nearestspot nearspot;
		Parkingspot pspot = null;
		for (int i = 0; i < parkinglist.size(); i++) {
			pspot = parkinglist.get(i).getParkingspot();
			nearspot = parkinglist.get(i);
			String tmptype = pspot.getsize();
			tmpspotid = pspot.getparkingID();
			int tmpvehparked = nearspot.getnumvehparked();

			if ((tmpspotid.compareTo(lower) >= 0) && (tmpspotid.compareTo(upper) <= 0)) {
				if (tmptype.equals("Large") && (tmpvehparked < 8)) {
					break;
				}
			}
			if ((tmpspotid.compareTo(lower) >= 0) && (tmpspotid.compareTo(upper) <= 0)) {
				if (tmptype.equals("Medium") && (tmpvehparked < 4)) {
					break;
				}
			}
		}
		return pspot;
	}

	public void updateforparking(Parkingspot pspot, String type) {

		String pspottype = pspot.getsize();
		List<Nearestspot> parkinglist = parking.get(pspottype);
		for (int i = 0; i < parkinglist.size(); i++) {
			Parkingspot tmppspot = parkinglist.get(i).getParkingspot();
			Nearestspot nearspot = parkinglist.get(i);
			System.out.println("Here1" + type + " " + pspottype);
			System.out.println("Here1" + tmppspot + " " + pspot);
			if (tmppspot.equals(pspot)) {
				if (pspottype.equals(type)) {

					parkinglist.remove(i);
					nearspot.increasevehparked();
					movetofull(nearspot);
					break;
				}
				if (pspottype.equals("Large")) {
					if (type.equals("Medium") && nearspot.getnumvehparked() < 1) {
						nearspot.increasevehparked();
						nearspot.settypevehparked(type);
					}
					if (type.equals("Medium") && nearspot.getnumvehparked() == 1) {
						parkinglist.remove(i);
						nearspot.increasevehparked();
						movetofull(nearspot);
						break;
					}
					if (type.equals("Small") && nearspot.getnumvehparked() == 7) {
						parkinglist.remove(i);
						nearspot.increasevehparked();
						movetofull(nearspot);
						break;
					}
					if (type.equals("Medium") && nearspot.getnumvehparked() < 7) {
						nearspot.increasevehparked();
						nearspot.settypevehparked(type);
					}

				}
				if (pspottype.equals("Medium")) {
					if (type.equals("Small") && nearspot.getnumvehparked() < 3) {
						nearspot.increasevehparked();
						nearspot.settypevehparked(type);
					}
					if (type.equals("Small") && nearspot.getnumvehparked() == 3) {
						parkinglist.remove(i);
						nearspot.increasevehparked();
						movetofull(nearspot);
						break;
					}
				}

			}
		}
	}

	public void updateforunparking(Parkingspot pspot, String type) {
		String pspottype = pspot.getsize();
		List<Nearestspot> parkinglist = parking.get(pspottype);
		Boolean found = false;
		for (int i = 0; i < parkinglist.size(); i++) {
			Parkingspot tmppspot = parkinglist.get(i).getParkingspot();
			Nearestspot nearspot = parkinglist.get(i);
			if (tmppspot.equals(pspot)) {
				//[TODO] Why increase; shuldn't be decrease
				nearspot.increasevehparked();
				found = true;
			}

		}
		if (!found) {
			movefromfull(pspot);
		}
	}

	public void movetofull(Nearestspot nearspot) {
		fullparking.add(nearspot);
	}

	public void movefromfull(Parkingspot pspot) {
		String pspottype = pspot.getsize();
		Nearestspot nearspot = null;
		List<Nearestspot> tmpparking;
		for (int i = 0; i < fullparking.size(); i++) {
			if (pspot.equals(fullparking.get(i).getParkingspot())) {
				nearspot = fullparking.get(i);
				fullparking.remove(i);
			}
		}
		if (nearspot != null) {
			tmpparking = parking.get(pspottype);
			nearspot.decreasevehparked();
			tmpparking.add(nearspot);
		}
	}

}
