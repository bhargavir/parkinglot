package testparkinglot;

import java.util.*;

public class Parkingfinder {

	// [TODO] The name NearestSpot doesn't sound appropriate here. NearestSpot
	// is typically an outcome of a algo logic
	List<Nearestspot> fullparking = new ArrayList<Nearestspot>();
    Largeparking largeparking;
    Mediumparking mediumparking;
    Smallparking smallparking;
    
	public Parkingfinder(List<Parkingspot> parkingspots) {

		largeparking = new Largeparking(getLargeParkingSpots(parkingspots));
		mediumparking = new Mediumparking(getMediumParkingSpots(parkingspots));
		smallparking = new Smallparking(getSmallParkingSpots(parkingspots));
		
		smallparking.setsuccessor(mediumparking);
		mediumparking.setsuccessor(largeparking);
	}

	private List<Nearestspot> getSmallParkingSpots(List<Parkingspot> parkingspots) {
		// TODO You can try java-8 filter expression on list
		List<Nearestspot> smallspots = new ArrayList<Nearestspot>();
		for (Parkingspot pspot : parkingspots){
			if (pspot.getsize().equals("Small")) {
				Nearestspot nearspot = new Nearestspot(pspot);
				smallspots.add(nearspot);
			}
		}
			return smallspots;
	}

	private List<Nearestspot> getMediumParkingSpots(List<Parkingspot> parkingspots) {
		List<Nearestspot> mediumspots = new ArrayList<Nearestspot>();
		for (Parkingspot pspot : parkingspots){
			if (pspot.getsize().equals("Medium")) {
				Nearestspot nearspot = new Nearestspot(pspot);
				mediumspots.add(nearspot);
			}
		}
			return mediumspots;
	}

	private List<Nearestspot> getLargeParkingSpots(List<Parkingspot> parkingspots) {
		List<Nearestspot> largeSpots = new ArrayList<Nearestspot>();
		for (Parkingspot pspot : parkingspots){
			if (pspot.getsize().equals("Large")) {
				Nearestspot nearspot = new Nearestspot(pspot);
				largeSpots.add(nearspot);

			}

		}
		return largeSpots;
	}

	public Parkingspot getnearestspot(String gate, String type) {
		Parkingspot pspot = null;
		if (type.equals("Large")) {
			pspot = largeparking.findspot(type, gate);
		}
		if (type.equals("Medium")) {
			pspot = mediumparking.findspot(type, gate);
		}
		if (type.equals("Small")) {
			pspot = smallparking.findspot(type, gate);
		
		}
		System.out.println(pspot);
		return pspot;
	}

	public void updateforparking(Parkingspot pspot, String type) {

		String pspottype = pspot.getsize();
		Nearestspot nspot  = null;;
		if (pspottype.equals("Large")){
			nspot = largeparking.forparking(pspot, type);
		}
		if (pspottype.equals("Medium")){
			nspot = mediumparking.forparking(pspot, type);
		}
		if (pspottype.equals("Small")){
			nspot = smallparking.forparking(pspot, type);
		}
		if (nspot !=null){
			movetofull(nspot);
		}
		
			}

	public void updateforunparking(Parkingspot pspot, String type) {
		String pspottype = pspot.getsize();
		boolean found = false;
		if (pspottype.equals("Large")){
			found = largeparking.forunparking(pspot, type);
			if (!found) {
				Nearestspot nspot = movefromfull(pspot);
				largeparking.addtolist(nspot);
			}
		}
		if (pspottype.equals("Medium")){
			found = mediumparking.forunparking(pspot, type);
			if (!found) {
				Nearestspot nspot = movefromfull(pspot);
				mediumparking.addtolist(nspot);
			}
		}
		if (pspottype.equals("Small")){
			found = smallparking.forunparking(pspot, type);
			if (!found) {
				Nearestspot nspot = movefromfull(pspot);
				smallparking.addtolist(nspot);
			}
		}
		
		
	}

	public void movetofull(Nearestspot nearspot) {
		fullparking.add(nearspot);
	}

	public Nearestspot movefromfull(Parkingspot pspot) {
		Nearestspot nspot = null;
		for (Nearestspot nearspot : fullparking){
			if (pspot.equals(nearspot.getParkingspot())) {
				fullparking.remove(nearspot);
				nspot = nearspot;
			}
		}
		return nspot;
	}

}
