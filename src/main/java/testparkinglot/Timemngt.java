package testparkinglot;
import java.util.*;
import java.util.concurrent.TimeUnit;

public class Timemngt {
	
	Vehicle veh;
	Date timein;
   
	public Timemngt(Vehicle veh){
		this.veh = veh;
		this.timein = new Date();
	}
	
	public Vehicle getVehicle(){
		return veh;
	}
	
   public int getduration(){
	   int duration;
	   Date currenttime = new Date();
	   duration = (int) TimeUnit.MICROSECONDS.toHours(currenttime.getTime() - timein.getTime());
       return duration;
   }
}
