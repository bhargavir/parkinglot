package testparkinglot;
import java.util.*;

public class Timekeeper {

	List<Timemngt> timekeeper;
	
	public Timekeeper(){
		timekeeper = new ArrayList<Timemngt>();
	}
	
	public void updatevehicletimein(Vehicle veh){
		Timemngt time = new Timemngt(veh);
		timekeeper.add(time);
	}
	
	
	public int vehduration(Vehicle veh){
		int duration = 0;
		//for (Timemngt t : timekeeper){
		//	Vehicle tmpveh = t.getVehicle();
		//	if (tmpveh.equals(veh)){
		//		duration = t.getduration();
		//		break;
		//	}
		//}
		for (int i=0;i<timekeeper.size();i++){
			Timemngt time = timekeeper.get(i);
			Vehicle tmpveh = time.getVehicle();
			if (tmpveh.equals(veh)){
				duration = time.getduration();
				break;
			}
		}
		return duration;
	}
}
