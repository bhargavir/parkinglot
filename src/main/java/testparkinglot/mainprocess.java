package testparkinglot;

public class mainprocess {

	public static void main(String[] args) {
	    UserInterface ui = new UserInterface();
        String opt = ui.getOption();
        
        while (!opt.equals("X")){	
            if (opt.equals("1")){
            	ui.parkoption();
            }
            if (opt.equals("2")){
            	ui.unparkoption();
            }
            if (opt.equals("X")){
            	ui.exitprocess();
            }
            opt = ui.getOption();
        }
        
	}

}
