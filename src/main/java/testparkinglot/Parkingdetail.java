package testparkinglot;
import java.util.*;

public class Parkingdetail {
	
	//[TODO] change field names wherever appropriate; use camel casing as well
       Parkingspot parkingSpot;
       List<Vehicle> vehiclespark;
       int numVehiclesparked;
       
       public Parkingdetail(Parkingspot pspot){
    	   parkingSpot = pspot;
    	   numVehiclesparked = 0;
    	   vehiclespark = new ArrayList<Vehicle>();
       }
       
       public Parkingspot getParkingspot(){
    	   return parkingSpot;
       }
       
       //[TODO] change method names --- in this case parkVehicle may be more clear. Revist all method names like that
       public void updateparkVehicle(Vehicle vehicle){
    	   vehiclespark.add(vehicle);
    	   numVehiclesparked ++;
       }
       
       public void updateunparkVehicle(Vehicle veh){
    	   
    	   String regnum = veh.getregNum();
    	 	   for (Vehicle i : vehiclespark){
    		   String tmpregnum = i.getregNum();
    		   if (tmpregnum.equals(regnum)) {
    			   vehiclespark.remove(i);
    		   }
     	  }
    	
    	  
    	 	   numVehiclesparked -- ;
       }
}
