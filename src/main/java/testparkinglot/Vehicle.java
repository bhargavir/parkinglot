package testparkinglot;

public class Vehicle {
      String vehicleType;
      String regNum;
      
      public Vehicle(String type, String regnum)
      {
    	  vehicleType = type;
    	  regNum = regnum;
      }
      
      public String getvehicleType(){
    	  return vehicleType;
      }
      
      public String getregNum(){
    	  return regNum;
      }
}

