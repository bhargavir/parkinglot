package testparkinglot;
import java.util.*;

public class UserInterface {
	
	Scanner scanner;
	Parkingfinder parkfinder;
	ParkingLedger parkledger;
	Timekeeper timekeeper;
	
	public UserInterface(){
		scanner = new Scanner(System.in);
		
		List<Parkingspot> parkingspots = new ArrayList<Parkingspot>();
		Parkingspot p1 = new Parkingspot("B01", "Large");
		parkingspots.add(p1);
		p1 = new Parkingspot("B21", "Large");
		parkingspots.add(p1);
		p1 = new Parkingspot("B32", "Large");
		parkingspots.add(p1);
		
		parkfinder = new Parkingfinder(parkingspots);
		parkledger = new ParkingLedger(parkingspots);
		timekeeper = new Timekeeper();
	}
	
	public String getOption(){
		
		String opt;
		System.out.println("Select the options for the operation you want ");
		System.out.println("Enter '1' to Park a Vehicle ");
		System.out.println("Enter '2' to Unpark a vehicle ");
		System.out.println("Enter 'X' to Exit ");
		opt = scanner.nextLine();
		return opt;
	}
	
	public void parkoption(){
		Parkingspot parkingspot;
		System.out.println("Enter Gate entered ");
		String gate = scanner.nextLine();
		System.out.println("Enter vehicle type ");
		String type = scanner.nextLine();
		parkingspot = parkfinder.getnearestspot(gate, type);
		System.out.println("Enter vehicle reg num ");
		String regnum = scanner.nextLine();
		Vehicle veh = new Vehicle(type,regnum);
		System.out.println("Till Here1" + parkingspot);
		parkledger.parkvehicle(parkingspot, veh);
		timekeeper.updatevehicletimein(veh);
		parkfinder.updateforparking(parkingspot,type);
		//call parking methods
	}
	
    public void unparkoption(){
		
		System.out.println("Enter vehicle type ");
		String type = scanner.nextLine();
		System.out.println("Enter vehicle reg num ");
		String regnum = scanner.nextLine();
		Vehicle veh = new Vehicle(type,regnum);
		System.out.println("Enter Parkingspot ");
		String parkingid = scanner.nextLine();
		Parkingspot pspot = new Parkingspot(type,parkingid);
		//call unparking methods
		parkledger.unparkvehicle(pspot, veh);
		parkfinder.updateforunparking(pspot, type);
		//time management
		int duration = timekeeper.vehduration(veh);
		//billing
		Billing bill = new Billing(veh);
		double amount = bill.getbillingamt(duration);
		System.out.println("Billing amount " + amount);
		
	}

   public void exitprocess(){
	 scanner.close();
   }
}
