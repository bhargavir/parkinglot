package testparkinglot;

public class Billing {
	
	Vehicle veh;
	
	public Billing(Vehicle veh){
		this.veh = veh;
	}
	
	public double getbillingamt(int duration){
		double amount=0;
		String type = veh.getvehicleType();
		if (type.equals("Bike")){
			amount = duration * 100;
		}
		if (type.equals("Car")){
			amount = duration * 150;
		}
		if (type.equals("Bus")){
			amount = duration * 200;
		}
		return amount;
	}

}
