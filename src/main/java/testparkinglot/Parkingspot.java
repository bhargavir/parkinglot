package testparkinglot;

public class Parkingspot {
	String parkingID;
	//[TODO] Size looks like a number; why is it a String; need a better name like type
	String size;
	
	public Parkingspot(String parkingid, String size){
		this.parkingID = parkingid;
		this.size = size;
	}
	
	public String getparkingID(){
		return parkingID;
	}
	
	public String getsize(){
		return size;
	}

}
