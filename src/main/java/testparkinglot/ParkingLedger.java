package testparkinglot;
import java.util.*;

public class ParkingLedger {

	List<Parkingdetail> parkingarray;
	
	public ParkingLedger(List<Parkingspot> parkingspots){
		//[TODO] You can set the size of the parkingarray list straightaway on construction as you already know the size of parkingspots list;
		parkingarray = new ArrayList<Parkingdetail>();
		//load parkingspots
		for (Parkingspot pspot : parkingspots){
			Parkingdetail parkdt = new Parkingdetail(pspot);
			parkingarray.add(parkdt);
		}
	
	}
	
	public void parkvehicle(Parkingspot pspot, Vehicle veh){
		Parkingspot tmpspot = null;
		for (Parkingdetail tmpspotdt : parkingarray){
			tmpspot = tmpspotdt.getParkingspot();
			if (tmpspot.equals(pspot))
			{
				tmpspotdt.updateparkVehicle(veh);
				break;
			}
		}
		
		//[TODO] how would you handle if for some reason the parking-spot input is not found?
	}
	
	public void unparkvehicle(Parkingspot pspot,Vehicle veh ){
		Parkingspot tmpspot;
		for (Parkingdetail tmpspotdt : parkingarray){
			tmpspot = tmpspotdt.getParkingspot();
			if (tmpspot.equals(pspot))
			{
				tmpspotdt.updateunparkVehicle(veh);
				break;
			}
	
		}
	}
}
