package testparkinglot;
import java.util.*;

public class Mediumparking extends Parkinglot {
	
	HashMap<String, Integer> vehalloweddetail = new HashMap<String, Integer>();
	HashMap<String, List<String>> neargate = new HashMap<String, List<String>>();
    String size;
    List<Nearestspot> parkinglist;

    public Mediumparking(List<Nearestspot> parkinglist){

     this.parkinglist = parkinglist;
     
     vehalloweddetail.put("Large", 0);
     vehalloweddetail.put("Medium", 1);
     vehalloweddetail.put("Small", 4);
     
     List<String> gaterange = new ArrayList<String>();
     gaterange.add("B1");
     gaterange.add("B10");
     gaterange.add("B11");
     gaterange.add("B20");
     gaterange.add("B21");
     gaterange.add("B30");
     neargate.put("A1",gaterange);
     List<String> gate1 = new ArrayList<String>();
     gate1.add("B11");
     gate1.add("B20");
     gate1.add("B1");
     gate1.add("B10");
     gate1.add("B21");
     gate1.add("B30");
     neargate.put("A2",gate1);
     List<String> gate2 = new ArrayList<String>();
     gate2.add("B21");
     gate2.add("B30");
     gate2.add("B11");
     gate2.add("B20");
     gate2.add("B1");
     gate2.add("B10");
     neargate.put("A3",gate2);
     
     size = "Medium";
     
    }
    
    public Parkingspot findspot(String type, String gate){
    	
    	Parkingspot retspot = null;
    	List<String> gaterange = new ArrayList<String>();
    	gaterange = neargate.get(gate);
    	if (type.equals(size)){
    	for (int i = 0;i<gaterange.size();i = i+2){
    	   		
        		String tmpspotid;
        		String lower = gaterange.get(i);
            	String upper = gaterange.get(i+1);
        		for (Nearestspot nearspot : parkinglist){
        			Parkingspot pspot = nearspot.getParkingspot();
        			tmpspotid = pspot.getparkingID();
        			if ((tmpspotid.compareTo(lower) >= 0) && (tmpspotid.compareTo(upper) <= 0)) {
        				retspot = pspot;
        				break;
        			} 
        		}
        		
        	}
      	}
    	
      if (!type.equals(size)){ 	
    	  System.out.println("In large parking");
    	for (int i = 0;i<gaterange.size();i = i+2){
    	
    		for (Nearestspot nearspot : parkinglist){
    			String lower = gaterange.get(i);
            	String upper = gaterange.get(i+1);
    			retspot = getspotfrompartial(type, lower,upper, nearspot);
    			if (retspot !=null){
    				break;
    			}
    		}
    	}
    	
    	if (retspot == null)
    	for (int i = 0;i<gaterange.size();i = i+2){
        	for (Nearestspot nearspot : parkinglist){
        			String lower = gaterange.get(i);
                	String upper = gaterange.get(i+1);
        			retspot = getspotfromfree(type, lower,upper, nearspot);
        			if (retspot !=null){
        				break;
        			}
        			
        		}
        	}
    	}
      if (retspot == null)
  	{
  		retspot = successor.findspot(type, gate);
  	}
    
  	return retspot;
    	
    	
    }

    private Parkingspot getspotfrompartial(String type, String lower, String upper, Nearestspot nearspot){
    	
    	Parkingspot pspot;
    	int vehallow = vehalloweddetail.get(type);
    	pspot = nearspot.getParkingspot();
		String tmpspotid = pspot.getparkingID();
    	if ((tmpspotid.compareTo(lower) >= 0) && (tmpspotid.compareTo(upper) <= 0) && vehallow > nearspot.getnumvehparked() && nearspot.getnumvehparked() > 0) {
    		return pspot;
		} else {
			return null;
		}
      }
    
    private Parkingspot getspotfromfree(String type, String lower, String upper, Nearestspot nearspot){
    	Parkingspot pspot;
    	int vehallow = vehalloweddetail.get(type);
    	pspot = nearspot.getParkingspot();
		String tmpspotid = pspot.getparkingID();
    	if ((tmpspotid.compareTo(lower) >= 0) && (tmpspotid.compareTo(upper) <= 0) && vehallow > nearspot.getnumvehparked()) {
    		return pspot;
		} else {
			return null;
		}
       }
    
    public Nearestspot forparking(Parkingspot pspot, String type){
	    Nearestspot nspot = null;
	   	for (Nearestspot nearspot : parkinglist){
		Parkingspot tmpspot = nearspot.getParkingspot();
		if (tmpspot.equals(tmpspot)){
			nearspot.settypevehparked(type);
			nearspot.increasevehparked();
			int vehallow = vehalloweddetail.get(type);
			if (vehallow == nearspot.getnumvehparked()){
				nspot = nearspot;
				parkinglist.remove(nearspot);
			}
			
		}
	}
	return nspot;
}
    public boolean forunparking(Parkingspot pspot, String type){
    	boolean found = false;
    	for (Nearestspot nearspot : parkinglist){
    		Parkingspot tmpspot = nearspot.getParkingspot();
    		if (tmpspot.equals(tmpspot)){
    			nearspot.decreasevehparked();
    			found = true;
    			break;
    		}
    	}
    	return found;
    }
    
    public void addtolist(Nearestspot nspot){
    	nspot.decreasevehparked();
		parkinglist.add(nspot);
    }

}
