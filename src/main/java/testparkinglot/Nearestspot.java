package testparkinglot;

public class Nearestspot {

	Parkingspot parkingspot;
	String typevehparked;
	int numofvehparked;
	
	public Nearestspot(Parkingspot pspot){
		parkingspot = pspot;
		typevehparked = "";
		numofvehparked = 0;
	}
	public Parkingspot getParkingspot(){
		return parkingspot;
	}
	
	public String gettypevehparked(){
		return typevehparked;
	}
	
	public int getnumvehparked(){
		return numofvehparked;
	}
	
	public void settypevehparked(String type){
		type = typevehparked; 
	}
	
	public void increasevehparked(){
		numofvehparked ++;
	}
	
	public void decreasevehparked(){
		numofvehparked --;
	}
}
